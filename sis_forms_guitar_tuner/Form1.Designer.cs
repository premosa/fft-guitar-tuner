﻿namespace sis_forms_guitar_tuner
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.graph_timeDomain = new ScottPlot.ScottPlotUC();
            this.graph_frequencyDomain = new ScottPlot.ScottPlotUC();
            this.FrequencyDomainLabel = new System.Windows.Forms.TextBox();
            this.TimeDomainLabel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Label_fundamentalFrequency = new System.Windows.Forms.Label();
            this.Label_frequencyIndex = new System.Windows.Forms.Label();
            this.tunerDiagram = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.tuningStatus = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // graph_timeDomain
            // 
            this.graph_timeDomain.Location = new System.Drawing.Point(12, 38);
            this.graph_timeDomain.Name = "graph_timeDomain";
            this.graph_timeDomain.Size = new System.Drawing.Size(946, 239);
            this.graph_timeDomain.TabIndex = 0;
            // 
            // graph_frequencyDomain
            // 
            this.graph_frequencyDomain.Location = new System.Drawing.Point(12, 341);
            this.graph_frequencyDomain.Name = "graph_frequencyDomain";
            this.graph_frequencyDomain.Size = new System.Drawing.Size(946, 217);
            this.graph_frequencyDomain.TabIndex = 1;
            // 
            // FrequencyDomainLabel
            // 
            this.FrequencyDomainLabel.Location = new System.Drawing.Point(51, 294);
            this.FrequencyDomainLabel.Name = "FrequencyDomainLabel";
            this.FrequencyDomainLabel.ReadOnly = true;
            this.FrequencyDomainLabel.Size = new System.Drawing.Size(896, 22);
            this.FrequencyDomainLabel.TabIndex = 2;
            // 
            // TimeDomainLabel
            // 
            this.TimeDomainLabel.Location = new System.Drawing.Point(51, 10);
            this.TimeDomainLabel.Name = "TimeDomainLabel";
            this.TimeDomainLabel.ReadOnly = true;
            this.TimeDomainLabel.Size = new System.Drawing.Size(896, 22);
            this.TimeDomainLabel.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(986, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Index of FFT data:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1066, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(207, 39);
            this.label2.TabIndex = 5;
            this.label2.Text = "Guitar Tuner";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(986, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(228, 24);
            this.label3.TabIndex = 6;
            this.label3.Text = "Fundamental Frequency: ";
            // 
            // Label_fundamentalFrequency
            // 
            this.Label_fundamentalFrequency.AutoSize = true;
            this.Label_fundamentalFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_fundamentalFrequency.Location = new System.Drawing.Point(1220, 165);
            this.Label_fundamentalFrequency.Name = "Label_fundamentalFrequency";
            this.Label_fundamentalFrequency.Size = new System.Drawing.Size(53, 24);
            this.Label_fundamentalFrequency.TabIndex = 7;
            this.Label_fundamentalFrequency.Text = "XYZ ";
            // 
            // Label_frequencyIndex
            // 
            this.Label_frequencyIndex.AutoSize = true;
            this.Label_frequencyIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_frequencyIndex.Location = new System.Drawing.Point(1220, 118);
            this.Label_frequencyIndex.Name = "Label_frequencyIndex";
            this.Label_frequencyIndex.Size = new System.Drawing.Size(53, 24);
            this.Label_frequencyIndex.TabIndex = 8;
            this.Label_frequencyIndex.Text = "XYZ ";
            // 
            // tunerDiagram
            // 
            this.tunerDiagram.Location = new System.Drawing.Point(964, 293);
            this.tunerDiagram.Name = "tunerDiagram";
            this.tunerDiagram.Size = new System.Drawing.Size(471, 23);
            this.tunerDiagram.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(986, 389);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "Status:";
            // 
            // tuningStatus
            // 
            this.tuningStatus.AutoSize = true;
            this.tuningStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tuningStatus.Location = new System.Drawing.Point(1057, 389);
            this.tuningStatus.Name = "tuningStatus";
            this.tuningStatus.Size = new System.Drawing.Size(48, 24);
            this.tuningStatus.TabIndex = 11;
            this.tuningStatus.Text = "XYZ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1028, 319);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "E2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1048, 275);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 15);
            this.label6.TabIndex = 13;
            this.label6.Text = "A2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1082, 319);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "D3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1179, 319);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 15);
            this.label8.TabIndex = 15;
            this.label8.Text = "B3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1129, 275);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 15);
            this.label9.TabIndex = 16;
            this.label9.Text = "G3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1257, 275);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "E4";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1447, 593);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tuningStatus);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tunerDiagram);
            this.Controls.Add(this.Label_frequencyIndex);
            this.Controls.Add(this.Label_fundamentalFrequency);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TimeDomainLabel);
            this.Controls.Add(this.FrequencyDomainLabel);
            this.Controls.Add(this.graph_frequencyDomain);
            this.Controls.Add(this.graph_timeDomain);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ScottPlot.ScottPlotUC graph_timeDomain;
        private ScottPlot.ScottPlotUC graph_frequencyDomain;
        private System.Windows.Forms.TextBox FrequencyDomainLabel;
        private System.Windows.Forms.TextBox TimeDomainLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Label_fundamentalFrequency;
        private System.Windows.Forms.Label Label_frequencyIndex;
        private System.Windows.Forms.ProgressBar tunerDiagram;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label tuningStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}

