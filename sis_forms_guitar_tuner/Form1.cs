﻿//Default includes
using System;
using System.Linq;
using System.Windows.Forms;

//My includes
using System.Runtime.InteropServices;

using NAudio.Wave;

using MathNet.Numerics;
using MathNet.Numerics.IntegralTransforms;

using MathNet.Filtering;


/// <summary>
/// STANDARD GUITAR TUNING / FREQUNCIES OF STRINGS
/// </summary>
//Strings
/*
 * E2 -  82.41Hz
 * A2 - 110.00Hz
 * D3 - 146.83Hz
 * G3 - 196.00Hz
 * B3 - 246.94Hz
 * E4 - 329.63Hz
 */

namespace sis_forms_guitar_tuner
{
    public partial class Form1 : Form
    {

        //variables which are responsible for capturing microphone data and updating graphs
        public WaveIn wi;
        public BufferedWaveProvider bwp;
        public Int32 envelopeMax;
        Timer MyTimer;

        static private int SAMPLERATE = 8000; // sample rate of the sound card (set it as low as possible, since guitar strings don't resonate with high frequency anyway; if possible, we could lower this to even something like 2kHz)
        static private int BUFFERSIZE = 1 << 12; // 2 to the power of 11 (we capture 1024 samples in time domain)
        static double FFTresolution = (double)SAMPLERATE / ((double)BUFFERSIZE / (double)BYTES_PER_POINT);

        static int SAMPLE_RESOLUTION = 16; //we will have int16 datatype
        static int BYTES_PER_POINT = SAMPLE_RESOLUTION / 8; //which means, we have 2 bytes per data

        OnlineFilter FIR_lowpass;
        OnlineFilter FIR_highpass;

        double[] windowData;

        // These variables get changed alot in loop / callbacks
        int fundamentalFrequencyIndex;
        double fundamentalFrequency;

        public Form1()
        {

            InitializeComponent();

            //Prepare GUI (labels,etc.)
            TimeDomainLabel.Text = "Normalized Time-Domain Data";
            FrequencyDomainLabel.Text = "Frequency-Domain Data -- Samplerate: " + SAMPLERATE + ", Bytes per data: " + BYTES_PER_POINT +  ", Buffersize(of data): " + BUFFERSIZE/2;

            //Set progress bar options (set default colour and max number of elements of progress bar)
            ModifyProgressBarColor.SetState(tunerDiagram, 2); //set default progressbar state to Error(we are not close to tuning)

            tunerDiagram.Maximum = (BUFFERSIZE/BYTES_PER_POINT) >> 3; //this is maximum value array "sum" can later achieve
            tunerDiagram.Maximum = tunerDiagram.Maximum >> 1; //to better represent the data, divide it by 2 again
            Console.WriteLine("tunerDiagram maximum: " + tunerDiagram.Maximum);

            Console.WriteLine("FFT resolution: " + FFTresolution);

            //get deviceCount (check how many we have; we will need to capture correct microphone)
            int devCount = WaveIn.DeviceCount;
            if (devCount < 1) {
                Console.WriteLine("ERROR: No input devices detected");
                return;
            }

            //prepare filter; frequency band is from 82.41Hz (E2) to 329.63Hz (E4)
            //customFIR = MathNet.Filtering.FIR.OnlineFirFilter.CreateBandpass(ImpulseResponse.Finite, SAMPLERATE, 70, 350); //give some margin for cutoff speed on low & high side
            FIR_highpass = MathNet.Filtering.FIR.OnlineFirFilter.CreateHighpass(ImpulseResponse.Finite,SAMPLERATE, 70); //Let frequencies above 70 be passed
            FIR_lowpass = MathNet.Filtering.FIR.OnlineFirFilter.CreateLowpass(ImpulseResponse.Finite, SAMPLERATE, 400); //Let frequencies bellow 400 be passed

            //prepare windowing data - one of our choice
            windowData = Window.Blackman(BUFFERSIZE/BYTES_PER_POINT);

            // get the WaveIn class started
            WaveIn wi = new WaveIn();
            wi.DeviceNumber = 0;
            wi.WaveFormat = new NAudio.Wave.WaveFormat(SAMPLERATE, 1);
            wi.BufferMilliseconds = (int)((double)BUFFERSIZE / (double)SAMPLERATE * 1000.0);

            // create a wave buffer and start the recording
            wi.DataAvailable += new EventHandler<WaveInEventArgs>(wi_DataAvailable);
            bwp = new BufferedWaveProvider(wi.WaveFormat);
            bwp.BufferLength = BUFFERSIZE * 2;

            //Start recording data
            bwp.DiscardOnBufferOverflow = true; //only capture current data
            wi.StartRecording();

            // create timer to plot captured data
            MyTimer = new Timer();
            MyTimer.Interval= (10); // 10 ms interval
            MyTimer.Tick += new EventHandler(MyTimer_Tick);
            MyTimer.Start();
        }

        // adds data to the audio recording buffer
        public void wi_DataAvailable(object sender, WaveInEventArgs e)
        {
            bwp.AddSamples(e.Buffer, 0, e.BytesRecorded);
        }

        private void MyTimer_Tick(object sender, EventArgs e)
        {
            //Process data from input audio samples and plot it in frequency domain, where we will compare how well it matches certain frequencies

            // read the bytes from the stream
            int frameSize = BUFFERSIZE;
            var frames = new byte[frameSize];
            bwp.Read(frames, 0, frameSize);

            if (frames.Length == 0) return;
            if (frames[frameSize - 2] == 0) return;

            // Pause timer so we can process data, before capturing new data
            MyTimer.Stop();

            // convert it to int32 manually (and a double for scottplot)
            int DATALENGTH = frames.Length / BYTES_PER_POINT;
            Int32[] vals = new Int32[DATALENGTH];
            double[] Ys = new double[DATALENGTH];
            double[] Xs = new double[DATALENGTH];
            double[] Ys2 = new double[DATALENGTH];
            double[] Xs2 = new double[DATALENGTH];

            //update data in X,Y (time domain) and X2,Y2 (frequency domain)
            {
                int i; for (i = 0; i < DATALENGTH; i++)
                {
                    // bit shift the byte buffer into the right variable format
                    byte hByte = frames[i * 2 + 1];
                    byte lByte = frames[i * 2 + 0];
                    vals[i] = (int)(short)((hByte << 8) | lByte);
                    Xs[i] = i;
                    Ys[i] = vals[i];
                    Xs2[i] = (double)i / Ys.Length * SAMPLERATE; // units are in kHz
                }
            }

            //apply filters for only certain frequencies
            {
                Ys = FIR_lowpass.ProcessSamples(Ys);
                Ys = FIR_highpass.ProcessSamples(Ys);
            }
            
            {
                //calculate maximum number of bits for signed version & calculate min and max value
                double bits = (double)(1 << (SAMPLE_RESOLUTION - 1));
                double minValue = -bits;
                double maxValue = bits - 1;
                double normalizationFactor = maxValue - minValue;
                //normalize data to range [-1,1]
                int i;
                for (i = 0; i < DATALENGTH; i++) {
                    Ys[i] = 2*( (Ys[i] - minValue) / (normalizationFactor) ) - 1;
                }
            }

            //Perform FFT on input data and return amplitudes, which will be used to determine present frequencies
            Ys2 = performFFT(Ys);

            //Ys2 is in frequency domain now, and first part of the graph shows amplitudes (since we only analyse the values, we don't need to iFFT; therefore there is no need for "proper" two-sided data manipulation of FFT)
            fundamentalFrequency = findFundamentalFrequency(Ys2);

            //limit fundamental frequency index at maximum of tunerDiagram maximum
            if (fundamentalFrequencyIndex > tunerDiagram.Maximum) {
                fundamentalFrequencyIndex = tunerDiagram.Maximum - 1;
            }

            Console.WriteLine("Fundamental frequency index: " + fundamentalFrequencyIndex);
            Console.WriteLine("Fundamental frequency: " + fundamentalFrequency);

            //update fundamental frequency and fundamental frequency index
            Label_fundamentalFrequency.Text = fundamentalFrequency.ToString();
            Label_frequencyIndex.Text = fundamentalFrequencyIndex.ToString();
            //We now have fundamental frequency; create if.. else.. automata for finding how to tune the guitar
            //this also updates progress bar visually
            analyseFundamentalFrequency();

            //Manually set axis range for time domain
            graph_timeDomain.SP.AxisSet(0, DATALENGTH, -1, 1);
            //Transfer Xs and Ys data into graph
            graph_timeDomain.Xs = Xs;
            graph_timeDomain.Ys = Ys;
            graph_timeDomain.UpdateGraph();

            //Manually set axis range for time domain
            //graph_frequencyDomain.SP.AxisSet(0, SAMPLERATE/2, 0, 5);

            //Manually set axis for frequency domain
            graph_frequencyDomain.Xs = Xs2;
            graph_frequencyDomain.Ys = Ys2;
            graph_frequencyDomain.UpdateGraph();

            // Start timer again to capture new samples
            MyTimer.Start();
        }

        private double[] performFFT(double[] Ys) {
            int length = Ys.Length;
            //prepare complex array
            Complex32[] FFTdata = new Complex32[length];
            int i;
            for (i = 0; i < length; i++)
            {
                
                Ys[i] = Ys[i] * windowData[i]; // Apply windowing
                // Save data in FFTdata (will be transformed into freq. domain)
                FFTdata[i] = (float)Ys[i];
            }

            //perform FFT
            Console.WriteLine("length: " + length);
            Fourier.Forward(FFTdata);

            int FFTlength = length >> 1; //divide by 2
            double[] result = new double[FFTlength];
            for (i = 0; i < FFTlength; i++) {
                result[i] = Math.Sqrt( Math.Pow(FFTdata[i].Real,2) + Math.Pow(FFTdata[i].Imaginary,2) );
            }

            //result already has 1/2 of array calculated magnitudes of present frequency
            return result;
        }

        private double findFundamentalFrequency(double[] Amplitudes) {
            int defaultLength = Amplitudes.Length; //defaultLength is only half of actual length (so when we search for fundamental frequency, MULTIPLY defaultLength with 2) - array Amlitudes is already cut half of FFT result
            int length = defaultLength >> 2; //divide length by 4 (right shift by 2)
            //Console.WriteLine("sum length: " + length);

            double[] sum = new double[length];
            {
                int i;
                for (i = 0; i < length; i++) {
                    sum[i] = Amplitudes[i] + Amplitudes[2 * i] + Amplitudes[3 * i]; //do sum of first 3 harmonics of given frequencies
                }
            }

            // Now among all sum values, find max Value
            double maxValue = sum.Max(); // Find max value in array of sum
            fundamentalFrequencyIndex = Array.IndexOf(sum, maxValue); //find & save fundamental frequency index

            //return value of fundamental frequency
            return ((double)fundamentalFrequencyIndex * SAMPLERATE / (defaultLength*2)); //return calculated fundamental frequency based on frequency resolution
        }

        private void analyseFundamentalFrequency() {
            if (fundamentalFrequencyIndex == 21 ||
                fundamentalFrequencyIndex == 28 ||
                fundamentalFrequencyIndex == 37 ||
                fundamentalFrequencyIndex == 50 ||
                fundamentalFrequencyIndex == 63 ||
                fundamentalFrequencyIndex == 84)
            {
                //WE ARE AT DESIRED FREQUENCY
                //then change progress colour to green
                ModifyProgressBarColor.SetState(tunerDiagram, 1); //1-green, 2-red, 3-yellow
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant
                if (fundamentalFrequencyIndex == 21)
                {
                    tuningStatus.Text = "Tuned string to E2";
                }
                else if (fundamentalFrequencyIndex == 28)
                {
                    tuningStatus.Text = "Tuned string to A2";
                }
                else if (fundamentalFrequencyIndex == 37)
                {
                    tuningStatus.Text = "Tuned string to D3";
                }
                else if (fundamentalFrequencyIndex == 50)
                {
                    tuningStatus.Text = "Tuned string to G3";
                }
                else if (fundamentalFrequencyIndex == 63)
                {
                    tuningStatus.Text = "Tuned string to B3";
                }
                else
                {
                    tuningStatus.Text = "Tuned string to E4";
                }
            }
            else if (fundamentalFrequencyIndex == 20 ||
                fundamentalFrequencyIndex == 19 ||
                fundamentalFrequencyIndex == 18
                )
            {
                //WE ARE RIGHT BELLOW E2
                //then change progress colour to yellow
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Increase frequency to tune string E2";
            }
            else if (fundamentalFrequencyIndex == 22 ||
                fundamentalFrequencyIndex == 23 ||
                fundamentalFrequencyIndex == 24)
            {
                //WE ARE RIGHT ABOVE E2
                //then change progress colour to yellow
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Decrease frequency to tune string E2";
            }
            else if (fundamentalFrequencyIndex == 27 ||
                fundamentalFrequencyIndex == 26 ||
                fundamentalFrequencyIndex == 25)
            {
                //WE ARE RIGHT BELLOW A2
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Increase frequency to tune string A2";
            }
            else if (fundamentalFrequencyIndex == 29 ||
                fundamentalFrequencyIndex == 30 ||
                fundamentalFrequencyIndex == 31 ||
                fundamentalFrequencyIndex == 32)
            {
                //WE ARE RIGHT ABOVE A2
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Decrease frequency to tune string A2";
            }
            else if (fundamentalFrequencyIndex == 36 ||
                fundamentalFrequencyIndex == 35 ||
                fundamentalFrequencyIndex == 34 ||
                fundamentalFrequencyIndex == 33)
            {
                //WE ARE RIGHT BELLOW D3
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Increase frequency to tune string D3";
            }
            else if (fundamentalFrequencyIndex == 38 ||
                fundamentalFrequencyIndex == 39 ||
                fundamentalFrequencyIndex == 40)
            {
                //WE ARE RIGHT ABOVE D3
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Decrease frequency to tune string D3";
            }
            else if (fundamentalFrequencyIndex == 49 ||
                fundamentalFrequencyIndex == 48 ||
                fundamentalFrequencyIndex == 47)
            {
                //WE ARE RIGHT BELLOW G3
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Increase frequency to tune string G3";
            }
            else if (fundamentalFrequencyIndex == 51 ||
                fundamentalFrequencyIndex == 52 ||
                fundamentalFrequencyIndex == 53)
            {
                //WE ARE RIGHT ABOVE G3
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Decrease frequency to tune string G3";
            }
            else if (fundamentalFrequencyIndex == 62 ||
                fundamentalFrequencyIndex == 61 ||
                fundamentalFrequencyIndex == 60)
            {
                //WE ARE RIGHT BELLOW B3
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Increase frequency to tune string B3";
            }
            else if (fundamentalFrequencyIndex == 64 ||
                fundamentalFrequencyIndex == 65 ||
                fundamentalFrequencyIndex == 66)
            {
                //WE ARE RIGHT ABOVE B3
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Decrease frequency to tune string B3";
            }
            else if (fundamentalFrequencyIndex == 83 ||
                fundamentalFrequencyIndex == 82 ||
                fundamentalFrequencyIndex == 81)
            {
                //WE ARE RIGHT BELLOW E4
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Increase frequency to tune string E4";
            }
            else if (fundamentalFrequencyIndex == 85 ||
                fundamentalFrequencyIndex == 86 ||
                fundamentalFrequencyIndex == 87)
            {
                //WE ARE RIGHT ABOVE E4
                ModifyProgressBarColor.SetState(tunerDiagram, 3);
                tunerDiagram.Value = fundamentalFrequencyIndex + 1;
                tunerDiagram.Value = fundamentalFrequencyIndex; //Change it twice so progress bar is instant

                tuningStatus.Text = "Decrease frequency to tune string E4";
            }
            else
            {
                //WE ARE NEITHER AT SEARCHED FREQUENCIES NOR CLOSE TO THEM
                //We are way above or way bellow values, change diagram to red
                ModifyProgressBarColor.SetState(tunerDiagram, 2);
                tunerDiagram.Value = fundamentalFrequencyIndex;
                if (fundamentalFrequencyIndex > 0)
                {
                    tunerDiagram.Value = fundamentalFrequencyIndex - 1;
                }

                tuningStatus.Text = "Unknown data";
            }
        }
    }

    /// <summary>
    /// Change progressbar look from error,warning and success, deppending how close to tuning we are
    /// </summary>
    public static class ModifyProgressBarColor
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr w, IntPtr l);
        public static void SetState(this ProgressBar pBar, int state)
        {
            SendMessage(pBar.Handle, 1040, (IntPtr)state, IntPtr.Zero);
        }
    }
}
